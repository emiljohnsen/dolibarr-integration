package App.properties;

import java.io.File;

public class ServerProperties extends Properties {

	/**
	 * Author: Rasmus Nim
	 * Edited: 18th May 2015
	 * Version: 1.0
	 * Description: Properties f�r ServerConection
	 */
	
	private final String TOKEN = "token";
	private final String URL_ADRESS = "url";
	
	public ServerProperties() {
		super(new File(Properties.PATH + Properties.DEFAULT_FILE));
		this.section = "server";
	}
	public ServerProperties token() { 
		this.property = this.TOKEN;
		return this;
	}
	public ServerProperties url() { 
		this.property = this.URL_ADRESS;
		return this;
	}
	public String get()
	{	
		return super.get().toString();
	}
}
