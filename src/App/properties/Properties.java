	/**
	   *Author: Emil
	   *Edited: 12 Maj 2015
	   *Version: 2.0
	   *Description: Abstrakt superklass f�r properties.
	 * 				Kommer ha concurrency-problem om tv� subklasser redigerar ini samtidigt.
	 * 				L�s i framtiden genom att l�sa in en kopia p� nytt, om det inte redan fungerar s�.
	 */

package App.properties;

import java.io.File;
import java.io.IOException;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

abstract public class Properties {
	public final static String PATH = System.getProperty("user.dir") + "/src/App/properties/";
	public final static String DEFAULT_FILE = "Properties.ini";
	
	protected Ini file;
	protected String property;
	protected String section;
	
	/**
	 * Author: Emil
	 * Edited: 11 Maj 2015
	 * Version: 1.0
	 * Description: Constructors f�r Properties. Tar emot en file.
	 *
	 * @param File f
	 */
	public Properties(File f)
	{
		this.read(f);
	}
	
	protected Properties read(File f){
		Ini ini = null;
		try {
			ini = new Ini(f);
		} catch (InvalidFileFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.file = ini;

		return this;
	}
	
	/**
	   *Author: Emil
	   *Edited: 11 Maj 2015
	   *Version: 2.0
	   *Description: Sparar aktuell ini-fil.
	 */
	public Properties store(){
		try {
			this.file.store();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this;
	}

	/**
	 * author: Emil
	 * edited: 11 Maj 2015
	 * version: 1.0
	 *
	 * @param value
	 * @return
	 */
	public Properties set(String value)
	{
		this.file.get(this.section).replace(this.property, value);
		return this;
	}

	/**
	 * author: Emil
	 * edited: 11 Maj 2015
	 * version: 1.0
	 *
	 * @return
	 */
	public Object get()	{ return this.file.get(this.section, this.property); }

}
