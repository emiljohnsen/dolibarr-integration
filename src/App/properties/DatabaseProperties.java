package App.properties;

import java.io.File;

public class DatabaseProperties extends Properties {
	
	/**
	 * Author: Rasmus Nim
	 * Edited: 18th May 2015
	 * Version: 1.0
	 * Description: Properties f�r databas i dolibarr
	 */
	
	private final String PASS = "password";
	private final String USER = "user";
	private final String HOST = "host";
	
	public DatabaseProperties() {
		super(new File(Properties.PATH + Properties.DEFAULT_FILE));
		this.section = "database";
	}
	
	public DatabaseProperties pass() { 
		this.property = this.PASS;
		return this;
	}
	public DatabaseProperties user() { 
		this.property = this.USER;
		return this;
	}
	public DatabaseProperties host() { 
		this.property = this.HOST;
		return this;
	}
	public String get()
	{	
		return super.get().toString();
	}
}
