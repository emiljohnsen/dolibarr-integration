package App.properties;

import java.io.File;

/**
 *Author: Emil
 *Edited: 11 Maj 2015
 *Version: 1.0
 *Description: Hanterar [leads] i en given ini-fil.
 */
public class LeadProperties extends Properties
{
	private final String MIN_LEADS = "min";
	private final String MAX_LEADS = "max";
	
	public LeadProperties() {
		super(new File(Properties.PATH + Properties.DEFAULT_FILE));
		this.section = "leads";
	}
	
	/**
	   *Author: Emil
	   *Edited: 11 Maj 2015
	   *Version: 1.0
	 */
	public LeadProperties maxLeads() { 
		this.property = this.MAX_LEADS;
		return this;
	}

	/**
	   * Author: Emil
	   * Edited: 11 Maj 2015
	   * Version: 1.0
	 */
	public LeadProperties minLeads() { 
		this.property = this.MIN_LEADS;
		return this;
	}

	/**
	 *Author: Emil
	 *Edited: 12 Maj 2015
	 *Version: 1.0
	 */
	public LeadProperties set(int value)
	{
		super.set(Integer.toString(value));
		return this;
	}
	
	@Override
	public Integer get()
	{	
		return Integer.parseInt(super.get().toString());
	}
	
	@Override
	public LeadProperties store()
	{
		super.store();
		return this;
	}
	

}