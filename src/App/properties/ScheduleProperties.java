package App.properties;

import java.io.File;
import java.util.*;

import App.helper.DateHelper;

/**
 *Author: Emil
 *Edited: 11 Maj 2015
 *Version: 1.0
 *Description: Hanterar [schema] i en given ini-fil.
 */
public class ScheduleProperties extends Properties
{
	public ScheduleProperties()
	{
		super(new File(Properties.PATH + Properties.DEFAULT_FILE));
		this.section = "schema";
	}
	public ScheduleProperties runTime()
	{
		this.property = "runtime";
		return this;
	}
	public ScheduleProperties cron()
	{
		this.property = "cron_cmd";
		return this;
	}

	public ScheduleProperties run_program_path()
	{
		this.property = "run_program_path";
		return this;
	}
	public ScheduleProperties run_scheduler_path()
	{
		this.property = "run_scheduler_path";
		return this;
	}
	public ScheduleProperties run_again_in()
	{
		this.property = "run_again_in";
		return this;
	}

	/**
	   * Author:  Emil
	   * Edited: 12 Maj 2015
	   * Version: 2.0
	   * Description: Tar emot en str�ng [HH[:MM[:SS]]] och en dag (Mon, Tue, Wed, Thu, Fri, Sat, Sun) och uppdaterar. 
	 */
	public ScheduleProperties set(String timeIn, String dayIn){
		// Time of day as [hh[mm[ss]]]
		String[] time = timeIn.split(":");
		String t = "";	
		
		// If some pieces are missing, add "00". 
		for ( int i = 0; i < 3; i++)
		{
			String sep = (i == 0) ? "" : ":";
			try 
			{
				t = t + sep + time[i];
			}
			catch (IndexOutOfBoundsException ie)
			{
				t = t + sep + "00";
			}
		}
		super.set(t + " " + dayIn);
		return this;
	}
	
	/**
	   *Author: Emil
	   *Edited: 12 Maj 2015
	   *Version: 2.0
	   *Returnerar n�sta k�rning av programmet, som datum. 
	 */
	public Date next() {
		Calendar run = Calendar.getInstance();
		Calendar now = Calendar.getInstance();
		now.set(now.MILLISECOND, 0);

		Map<Integer, Integer> times = new DateHelper().toMap(this.get());
		Set<Integer> keys = times.keySet();

		for (Integer key : keys)
		{
			run.set(key, times.get(key));
		}

		if (run.before(now) )
			run.add(Calendar.WEEK_OF_YEAR, 1);

		return run.getTime();
	}
	
	@Override
	public String get()
	{
		return super.get().toString();
	}
}
