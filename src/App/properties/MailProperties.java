package App.properties;

import java.io.File;

/**
 *Author: Emil
 *Edited: 11 Maj 2015
 *Version: 1.0
 *Description: Hanterar [e-post] i en given ini-fil.
 */
public class MailProperties extends Properties 
{

	private static final String FROM = "from";
	private static final String REGEX_TRIM = "\\s*,\\s*";
	private static final String E_MAIL = "e-post";
	private static final String BLANK_STRING = "";
	private static final String HOST = "host";
	private static final String PASSWORD = "password";
	private static final String PORT = "port";

	private static String getMailAddressIsolationPattern(String mail)
	{
		return "\\s*\\b" + mail + "\\b\\s*(,|$)";
	}

	public MailProperties()
	{
		super(new File(Properties.PATH + Properties.DEFAULT_FILE));
		this.section = E_MAIL;
	}
	/**
	   * Author: Emil
	   * Edited: 11 Maj 2015
	   * Version: 2.0
	   * Description: S�ker och tar bort v�rde fr�n lista. G�r inga kollar p� str�ngen. 
	 */
	public MailProperties remove(String mailIn){
		this.set(this.get().replaceAll(getMailAddressIsolationPattern(mailIn), BLANK_STRING));
		return this;
	}
	
	/**
	   * Author: Rasmus och Emil
	   * Edited: 12 Maj 2015
	   * Version: 1.0
	   * Description: L�gger till kommaseparerat v�rde till maillista. Kollar ingen indata.
	 */
	public MailProperties add(String mailIn){
		this.set(this.get() + ", " + mailIn.toLowerCase());
		return this;
	}

	/**
	 * Author: Emil
	 * Edited: 11 Maj 2015
	 * Version: 1.0
	 * Description: S�tter vilken lista som manipuleras.
	 */
	public MailProperties list(String listIn)
	{
		this.property = listIn;
		return this;
	}

	public MailProperties from()
	{
		this.property = FROM;
		return this;
	}

	/**
	 * Author: Emil
	 * Edited: 11 Maj 2015
	 * Version: 1.0
	 * Description: G�r en komma-separerad str�ng till en array.
	 */
	public String[] toArray(){
		return this.get().trim().split(REGEX_TRIM);
	}
	
	public String get(int index)
	{
		return this.toArray()[index];
	}
	
	@Override
	public String get()
	{
		return super.get().toString();
	}

	public MailProperties host() {
		this.property = HOST;
		return this;
	}

	public MailProperties password() {
		this.property = PASSWORD;
		return this;
	}

	public MailProperties port() {
		this.property = PORT;
		return this;
	}

	public MailProperties auth() {
		this.property = "auth";
		return this;
	}

	public MailProperties starttls() {
		this.property = "starttls";
		return this;
	}

	public MailProperties sender_name() {
		this.property = "sender_name";
		return this;
	}
}
