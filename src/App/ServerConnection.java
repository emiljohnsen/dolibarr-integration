package App;

import java.io.*;
import java.net.*;

import javax.xml.parsers.*;
import org.w3c.dom.*;

import org.xml.sax.InputSource;

import App.properties.ServerProperties;

public class ServerConnection {

	/**
	 *Author: Rasmus Nim
	 *Edited: 11st Maj 2015
	 *Version: 1.0
	 *Description: Return xmldocument from webscraper
	 * @return org.w3c.dom.Document
	 */
	public Document getXmlFromLeads(){
		try{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new InputSource(new StringReader( getStringFromServer())));
		return doc;
		}catch(Exception e){
			
		}
		return null;
	}

	/**
	 *Author: Rasmus Nim
	 *Edited: 11st Maj 2015
	 *Version: 1.0
	 *Description: Return a xml string from server
	 * @return String med xmldata
	 * @throws Exception
	 */
	public String getStringFromServer() throws Exception {
		
		ServerProperties prop = new ServerProperties();
 
		URL obj = new URL(prop.url().get());
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Authorization: ", prop.token().get());
 
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		return response.toString();
	}
}
