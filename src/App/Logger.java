package App;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import App.AppException.ErrorCode;

/**
 *Author: Jonathan Lind
 *Edited: 5th May 2015
 *Version: 1.0
 *Description: Logger class that will be called from other classes whenever an event needs to be logged in the system log file.
 */
public class Logger {

    /**
     *Author: Jonathan Lind
     *Edited: 5th May 2015
     *Version: 1.0
     *Description: Writes a log to the current day's log file
     */
    public static void append(Severity sevIn, String msgIn, AppException.ErrorCode codeIn){

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String src = stackTraceElements[2].getClassName() + "." + stackTraceElements[2].getMethodName();

        String log = getDateString() +
                    getSourceString(src) +
                    getCodeString(codeIn) +
                    getSeverityString(sevIn.toString()) +
                    msgIn;

        try
        {
            FileWriter fw = new FileWriter(getFilename(),true);
            fw.write(System.getProperty( "line.separator" ));
            fw.write(log + "\n");
            fw.close();

        }
        catch(IOException ioe)
        {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

    /**
     *Author: Jonathan Lind
     *Edited: 5th May 2015
     *Version: 1.0
     *Description: Builds the string to add to the log file
     */
    public static String setString(String srcIn, String severIn, String msgIn, AppException.ErrorCode errIn){
        return getDateString() + getSourceString(srcIn) + getCodeString(errIn) + getSeverityString(severIn) + msgIn;
    }

    public static String getCodeString(ErrorCode errIn) {
        return "[" + errIn.getValue() + "] ";
    }

    /**
     *Author: Jonathan Lind
     *Edited: 5th May 2015
     *Version: 1.0
     *Description: Returns the date and time in correct format
     */
    public static String getDateString(){
        return "[" + LocalDateTime.now().toString() + "] ";
    }

    /**
     *Author: Jonathan Lind
     *Edited: 5th May 2015
     *Version: 1.0
     *Description: Returns the severity level in correct format
     */
    public static String getSeverityString(String sevIn){
        return "[" + sevIn + "] ";
    }

    /**
     *Author: Jonathan Lind
     *Edited: 5th May 2015
     *Version: 1.0
     *Description: Returns the source of call in correct format
     */
    public static String getSourceString(String sourceIn){
        return "[" + sourceIn + "] ";
    }

    /**
     *Author: Jonathan Lind
     *Edited: 5th May 2015
     *Version: 1.0
     *Description: Return the filename based on date
     */
    public static String getFilename() {
        return LocalDate.now().toString() + ".txt";
    }

    /**
     *Author: Jonathan Lind
     *Edited: 5th May 2015
     *Version: 1.0
     *Description: Defines ENUM values for severity
     */
    public enum severity {
        DEBUG(1), INFO(2), WARNING(3), FATAL(4);

        private final int id;
        severity(int id) { this.id = id; }
        public int getValue() { return id; }
    }

}
