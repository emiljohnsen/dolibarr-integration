package App;


public class Query {
	public StringBuilder values = new StringBuilder();
	public StringBuilder variables = new StringBuilder();
	public String table = "dolibarr.llx_societe";
	private String where;
	private String limit;

	/**
	 * Author: Rasmus Nim
	 * Edited: 19th May 2015
	 * Version: 1.0
	 * Description: puts variables in queryn, exempel (variabeln1) VALUES('value1')
	 * @param variableIn variabeln man vill stoppa in
	 * @param valueIn v�rdet f�r variablen
	 * @return this
	 */
	public Query appendValue(String variableIn, String valueIn){
		if(values.length()<1){
			values.append("'"+valueIn +"'");
			variables.append(variableIn);
		}else{
			values.append(",'"+valueIn +"'");
			variables.append(","+variableIn);
		}
		return this;
	}
	
	/**
	 * Author: Rasmus Nim
	 * Edited: 19th May 2015
	 * Version: 1.0
	 * Description: Chooses table
	 * @param tableIn tablename
	 * @return this
	 */
	public Query setTable(String tableIn){
		this.table = tableIn;
		return this;
	}
	/**
	 * Author: Rasmus Nim
	 * Edited: 19th May 2015
	 * Version: 1.0
	 * Description: return a insert string
	 * @return query f�r inserta saker
	 */
	public String getInsertQuery(){
		return "INSERT INTO " + this.table + " ("+ this.variables + ") VALUES ( "+this.values+")";
	}
	
	public String toSelect = "*";
	public Query setVariableToSelect(String toSelectIn){
		this.toSelect = toSelectIn;
		return this;
	}
	public Query setVariableToSelect(String[] toSelectIn)
	{
		this.toSelect = String.join(", ", toSelectIn);
		return this;
	}

	public String getSelectQuery(){
		String query = "SELECT " + this.toSelect +" FROM " + this.table;
		if (this.where != null) query += " WHERE" + this.where;
		if (this.limit != null) query += this.limit;
		return query.trim();
	}

	public Query count(String column) {
		this.toSelect = "COUNT(" + String.join(", ", column) + ")";
		return this;
	}

	public Query where(String column, String value)
	{
		if (this.where == null) this.where = "";
		else this.where += " AND";

		this.where += " '" + column + "' = '" + value + "'";
		return this;
	}

	public Query limit(int i) {
		this.limit = " LIMIT " + Integer.toString(i);
		return this;
	}
	
	/**
	* Author: Rasmus Nim
	 * Edited: 26th May 2015
	 * Version: 1.0
	 * Description: return a delete query
	 * @param statement �r det som st�r efter Where i queryn
	 * @return String
	 */
	public String getDeleteQuery(String statement){
		return "DELETE FROM " + this.table + " WHERE " + statement;
	}
	
}


