package App;
import java.sql.*;
import java.util.Properties;

import App.properties.DatabaseProperties;

public class Database {
	
	public Connection conn ;
	 
	public Database(){
		try {
			this.conn = connect();
		} catch (Exception e) {
			Logger.append(Severity.HIGH, "no connection to dolibarr", AppException.ErrorCode.DOLIBARR_CON_FAIL);
		}
	}
	 /**
	  * Author: Rasmus Nim
	 * Edited: 19th May 2015
	 * Version: 1.0
	 * Description: Try to connect to databse
	  * @return Connection till databasen
	  * @throws Exception
	  */
	public Connection connect() throws Exception {
		DatabaseProperties propIni = new DatabaseProperties();
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Properties prop = new Properties();
		prop.setProperty("user", propIni.user().get());
		prop.setProperty("password", propIni.pass().get());
		Connection conn = DriverManager.getConnection(propIni.host().get(),prop);
		conn.setCatalog("dolibarr");
		return conn;
	}

/**
 * Author: Rasmus Nim
	 * Edited: 19th May 2015
	 * Version: 1.0
	 * Description: Send updatequery to database
 * @param queryIn
 * @return boolean om den lyckas eller itne
 */
	public boolean sendInsertQuery(String queryIn){
		try{
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(queryIn);
			return true;
		}catch(Exception e){
			return false;
		}
	}

	/**
	 * Author: Rasmus Nim
		 * Edited: 19th May 2015
		 * Version: 1.0
		 * Description: Send sendequery to database
	 * @param queryIn
	 * @return boolean om den lyckas eller itne
	 */
	public ResultSet sendSelectQuery(String query)
	{
		try {
			return  conn.createStatement().executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Author: Rasmus Nim
	 * Edited: 26th May 2015
	 * Version: 1.1
	 * Description: try to send all leads to databse
	 * @param leadsIn
	 * @return AppException med info om hur det g�tt
	 */
	public AppException insertLeadsToDatabase(Lead[] leadsIn){
		int counter = 0;
		AppException.ErrorCode code = AppException.ErrorCode.SUCCESS;
		for (Lead l: leadsIn){
			if(!this.sendInsertQuery(new Query()
				.appendValue("nom",l.name)
				.appendValue("address", l.adress)
				.appendValue("email", l.email)
				.appendValue("zip", l.zip)
				.appendValue("town", l.city)
				.appendValue("siren", l.contact) //id prof 1
				.appendValue("phone", l.tele)
				.appendValue("siret", l.size) // id prof 2
				.appendValue("ape", l.currentProvider) // id prof 3
				.appendValue("client", "2") 		//f�r att bli lead och inte kund 2=lead 1=kund
				.getInsertQuery())){
				counter++;
			}
		}
		Severity sev = Severity.NON;
		if(leadsIn.length-counter < 10){
			code = AppException.ErrorCode.LOW_LEADS;
			sev = Severity.MEDIUM;
		}
		return new AppException(sev,leadsIn.length-counter + " leads av " +leadsIn.length+ " has been added to database ",code );
	}
	/**
	 * Author: Rasmus Nim
	 * Edited: 26th May 2015
	 * Version: 1.1
	 * Description: Delete all leads from dolibar
	 * @return
	 */
	public AppException deleteAllLeads(){
		if(this.sendInsertQuery(new Query().getDeleteQuery("client = '2'"))){
			return new AppException(Severity.NON, "Old leads deleted", AppException.ErrorCode.SUCCESS);
		}
		else{
			return new AppException(Severity.MEDIUM, "Could not delete leads from dolibarr", AppException.ErrorCode.DOLIBARR_BULK_FAIL);
		}
	}
	
}
