package App.mail.test;

import App.mail.Mail;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Map;

import static org.junit.Assert.*;

public class MailTest {
    private static final String MAIL_LIST_AS_STRING = "d, e,rasmus@cool.nu, test";
    private static final String FROM = "donotreply@customsolutions.se";
    private static final String EXISTING_RECIPIENT_LIST = "lista1";
    private static final String TESTSUBJECT = "Testsubject";
    private static final String TESTMESSAGE = "Test message";
    private static final String[] TESTRECIPIENTS = new String[] {"Testrecipient1", "Testrecipient2"};
    private static final String[] ACTUAL_RECIPIENT = new String[] {"johnsen.emil@gmail.com"};
    private static final String TEST_SENDER = "donotreply@customsolutions.se";
    private final FakeMailProperties fake_mail_properties = new FakeMailProperties();

    @Before
    public void setUp() throws Exception {
       // Mail mail = new Mail();
    }

    @Rule
    public ExpectedException ee0 = ExpectedException.none();
    @Test
    public void getMailList_NonExistingList_ShouldThrowException()
    {
        ee0.expect(NullPointerException.class);
        String[] receiver_list = fake_mail_properties.list("").toArray();
    }

    @Test
    public void getMailList_ExistingList_ShouldReturnListAsString()
    {
        FakeMailProperties fake_mail_properties = new FakeMailProperties();
        String receiver_list = fake_mail_properties.list(EXISTING_RECIPIENT_LIST).get();

        assertEquals(MAIL_LIST_AS_STRING, receiver_list);
    }

    @Test
    public void getSender_NoArg_ShouldReturnSender()
    {
        String from = fake_mail_properties.from().get();
        assertEquals(FROM, from);
    }

    @Rule
    public ExpectedException ee1 = ExpectedException.none();
    @Test
    public void createMail_NoArgs_ThrowException() throws NullPointerException {
        ee1.expect(NullPointerException.class);
        boolean mail = new Mail().send();
    }



    @Rule
    public ExpectedException ee3 = ExpectedException.none();
    @Test
    public void createMail_NoMessage__ThrowNoMessageException() throws NullPointerException {
        ee3.expect(NullPointerException.class);
        boolean mail = new Mail().subject(TESTSUBJECT).send();
    }

    @Rule
    public ExpectedException ee4 = ExpectedException.none();
    @Test
    public void createMail_NoRecipient_ThrowNoRecipientException() throws NullPointerException {
        ee4.expect(NullPointerException.class);
        boolean mail = new Mail()
                .subject(TESTSUBJECT)
                .message(TESTMESSAGE)
                .send();
    }

    @Rule
    public ExpectedException ee5 = ExpectedException.none();
    @Test
    public void createMail_NoSender_ThrowException() throws NullPointerException {
        ee5.expect(NullPointerException.class);
        boolean mail =  new Mail()
                    .subject(TESTSUBJECT)
                    .message(TESTMESSAGE)
                    .to(TESTRECIPIENTS)
                    .send();
    }

    @Test
    public void sendMail_AllArgs_True()
    {
        // F�rs�ker skicka mail. Det �r d�ligt.

    /*    assertTrue(new Mail()
                .subject("Re: Ngt �mne")
                .message("det �r fel p� ngt")
                .to(ACTUAL_RECIPIENT)
                .from(TEST_SENDER)
                .send());
*/

    }

    private class FakeMailProperties {
        private String property;
        public FakeMailProperties()
        {

        }

        public FakeMailProperties from()
        {
            this.property = FROM;
            return this;
        }

        public String get()
        {
            return this.property;
        }

        public FakeMailProperties list(String list)
        {
            if (list != EXISTING_RECIPIENT_LIST)
                throw new NullPointerException();
            else
                this.property = MAIL_LIST_AS_STRING;

            return this;
        }

        public String[] toArray()
        {
            return "d, e,rasmus@cool.nu, test".trim().split("\\s*,\\s*");
        }

    }


}