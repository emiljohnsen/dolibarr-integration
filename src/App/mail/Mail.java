package App.mail;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import App.AppException.ErrorCode;
import App.Logger;
import App.Severity;
import App.properties.MailProperties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {

    private static final String BLANK_STRING = "";
    private static final ErrorCode NO_SUBJECT_ERROR_CODE = null;
    private static final ErrorCode NO_MESSAGE_ERROR_CODE = null;
    private static final ErrorCode NO_RECIPIENTS_ERROR_CODE = null;
    private static final ErrorCode ENCODING_EXCEPTION_ERROR_CODE = null;
    private static final ErrorCode MESSAGING_EXCEPTION_ERROR_CODE = null;
    private static final ErrorCode ADDRESS_EXCEPTION_ERROR_CODE = null;
    private static final String NO_RECIPIENTS_ERROR_MESSAGE = "Mottagare saknas";
    private static final String NO_MESSAGE_ERROR_MESSAGE = "Meddelande saknas";
    private static final String NO_SUBJECT_ERROR_MESSAGE = "Rubrik saknas";
    private static final String ADDRESS_EXCEPTION_ERROR_MESSAGE = "Fel i mottagarnas e-postadresser";
    private static final String MESSAGEING_EXCEPTION_ERROR_MESSAGE = "Fel uppstod n�r meddelandet skulle skickas";
    private static final String ENCODING_EXCEPTION_ERROR_MESSAGE = "Teckenkodningsfel: Teckenkodning st�ds ej.";

    private String subject;
    private String message = BLANK_STRING;
    private String[] recipients;
    private String sender;
    private String host;
    private String password;
    private String sender_name;

    private Properties props = new Properties();

    public Mail()
    {
        // Set all properties here.
        MailProperties mail_props = new MailProperties();

        this.sender = mail_props.from().get();
        this.host = mail_props.host().get();
        this.password = mail_props.password().get();
        this.sender_name = mail_props.sender_name().get();

        // Properties som beh�vs f�r Gmail - anpassa efter aktuell mailserverkonfiguration
        this.props.put("mail.smtp.host", host);
        this.props.put("mail.smtp.user", mail_props.from().get());
        this.props.put("mail.smtp.password", this.password);
        this.props.put("mail.smtp.port", mail_props.port().get());
        this.props.put("mail.smtp.auth", mail_props.auth().get());
        this.props.put("mail.smtp.starttls.enable", mail_props.starttls().get());
    }

    public Mail subject(String subject) {
        this.subject = subject;
        return this;
    }

    public Mail message(String message) {
        this.message = message;
        return this;
    }

    public Mail appendMessage(String message)
    {
        this.message += message;
        return this;
    }

    public Mail to(String[] recipient) {
        this.recipients = recipient;
        return this;
    }

    public Mail from(String sender) {
        this.sender = sender;
        return this;
    }


    public boolean send()
    {
        if (this.subject == null)
        {
            Logger.append(Severity.HIGH, NO_SUBJECT_ERROR_MESSAGE, NO_SUBJECT_ERROR_CODE);
            throw new NullPointerException();
        }
        if (this.message == null)
        {
            Logger.append(Severity.HIGH, NO_MESSAGE_ERROR_MESSAGE, NO_MESSAGE_ERROR_CODE);
            throw new NullPointerException();
        }
        if (this.recipients == null)
        {
            Logger.append(Severity.HIGH, NO_RECIPIENTS_ERROR_MESSAGE, NO_RECIPIENTS_ERROR_CODE);
            throw new NullPointerException();
        }

        Session session = Session.getDefaultInstance(this.props, null);


        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(this.sender, this.sender_name));

            for (String to : this.recipients)
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to, ""));

            msg.setSubject(this.subject);
            msg.setText(this.message);

            Transport transport = session.getTransport("smtp");
            transport.connect(host, sender, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();

        } catch (AddressException e) {
            Logger.append(Severity.HIGH, ADDRESS_EXCEPTION_ERROR_MESSAGE, ADDRESS_EXCEPTION_ERROR_CODE);
        } catch (MessagingException e) {
            Logger.append(Severity.HIGH, MESSAGEING_EXCEPTION_ERROR_MESSAGE,MESSAGING_EXCEPTION_ERROR_CODE);
        } catch (UnsupportedEncodingException e) {
            Logger.append(Severity.HIGH, ENCODING_EXCEPTION_ERROR_MESSAGE, ENCODING_EXCEPTION_ERROR_CODE);
        }
        return true;
    }
    public Mail send(String subject, String message, String[] to)
    {
        this.subject(subject).message(message).to(to).send();
        return this;
    }
}
