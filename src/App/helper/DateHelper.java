package App.helper;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DateHelper {

    private final Map<String, Integer> days = new HashMap<String, Integer>()
    {{
        put("Sun", 1);
        put("Mon", 2);
        put("Tue", 3);
        put("Wed", 4);
        put("Thu", 5);
        put("Fri", 6);
        put("Sat", 7);
    }};

    public DateHelper()
    {
    }

    /**
     * Author: Emil
     * Edited: 12 Maj 2015
     * Version: 1.0
     * Description: Accepts a parameter time_day, formatted as HH:MM:SS Day
     *
     * @param time_day
     * @return
     */
    public Map<Integer, Integer> toMap(String time_day)
    {
        String[] time_day_list = time_day.split(" ");
        String[] time = time_day_list[0].split(":");

        return new HashMap<Integer, Integer>()
        {{
            put(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
            put(Calendar.MINUTE, Integer.parseInt(time[1]));
            put(Calendar.SECOND, Integer.parseInt(time[2]));
            put(Calendar.HOUR_OF_DAY, days.get(time_day_list[1]));
        }};

    }
}
