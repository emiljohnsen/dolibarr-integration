package App;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import App.properties.LeadProperties;

public class Leads {
	public Map<String, Lead> dict;
	public int countDup = 0;

	public int countInval = 0;
	private List<Lead> duplicate_leads = new ArrayList<Lead>();

	private List<String> invalid_mails = new ArrayList<String>();
	private List<String> invalid_names = new ArrayList<String>();
	private List<String> invalid_phones = new ArrayList<String>();
	private List<String> invalid_zips = new ArrayList<String>();

	public int countInvalMail = 0;
	public int countInvalName = 0;
	public int countInvalPhone = 0;
	public int countInvalZip = 0;

	/**
	 * Author: Rasmus Nim 
	 * Edited: 11th May 2015 
	 * Version: 1.0 Description:
	 * Constructor that creates a new hashtable of leads with email as key
	 */
	public Leads() {
		dict = new Hashtable<String, Lead>();
	}

	/**
	 * Author: Rasmus Nim 
	 * Edited: 11th May 2015 
	 * Version: 1.0 
	 * Description: Checks for duplicate entries in the leads list
	 * 
	 * @param emailIn
	 *            Mail address that will be used to check for duplicate_leads
	 * @return Boolean to indicate if there was a duplicate entry
	 */
	public boolean checkDuplicate(String emailIn) {
		return dict.containsKey(emailIn);
	}

	/**
	 * Author: Jonathan Lind 
	 * Edited: 25th May 2015 
	 * Version: 1.0 Description:
	 * Checks if email address is in valid format
	 * 
	 * @param emailIn
	 *            Mail address that will be validated
	 * @return Boolean to indicate if address was valid
	 */
	public boolean verifyEmail(String emailIn) {
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w a-z���]+\\.)+[\\w]+[\\w]$";
		return emailIn.matches(EMAIL_REGEX);
	}

	/**
	 * Author: Jonathan Lind 
	 * Edited: 25th May 2015 
	 * Version: 1.0 Description:
	 * Checks if name is in valid format
	 * 
	 * @param nameIn
	 *            name that will be validated
	 * @return Boolean to indicate if name was valid
	 */
	public boolean verifyName(String nameIn) {
		int size = nameIn.length();
		char[] name = nameIn.toCharArray();
		for (int i = 0; i < size; i++) {
			if (Character.isAlphabetic(name[i]) || Character.isWhitespace(name[i]) || name[i] =='-') {
			} else {
				return false;
			}
		}

		return true;
	}

	/**
	 * Author: Jonathan Lind 
	 * Edited: 26th May 2015 
	 * Version: 1.0 Description:
	 * Checks if phone number is in valid format
	 * 
	 * @param zipIn
	 *            phone number that will be validated
	 * @return Boolean to indicate if phone number was valid
	 */
	public boolean verifyPhone(String phoneIn) {

		// String PHONE_REGEX = "^\\+?\\(*\\)?[\\d\\s\\-]";
		String PHONE_REGEX = "^[\\+?\\(*\\d\\)\\-\\s*]*$"; 
		return phoneIn.matches(PHONE_REGEX);
	}

	/**
	 * Author: Jonathan Lind 
	 * Edited: 25th May 2015 
	 * Version: 1.0 Description:
	 * Checks if zip code is in valid format
	 * 
	 * @param zipIn
	 *            zip code that will be validated
	 * @return Boolean to indicate if zip code was valid
	 */
	public boolean verifyZip(String zipIn) {
		if (zipIn.length() > 7) {
			return false;
		}
		String zip = zipIn.replace("/\\s/g", "");
		String ZIP_REGEX = "^\\d*$";
		return zip.matches(ZIP_REGEX);
	}

	/**
	 * Author: Rasmus Nim 
	 * Edited: 11th May 2015 
	 * Version 1.0 
	 * Description: Removes
	 * a lead
	 * 
	 * @param emailIn
	 *            Mail address that will be used to locate the entry that will
	 *            be removed
	 */
	public void removeLeadByKey(String emailIn) {
		dict.remove(emailIn);
	}

	// n�r man addar till listan med en key som redan finns skrivs befintlig
	// �ver.
	/**
	 * Author: Rasmus Nim 
	 * Edited: 1th June 2015 
	 * Version: 1.1 
	 * Description: Adds a
	 * lead to the leads-list and overwrites any duplicate_leads
	 * 1.1 kollar data innan den l�gger in i dict
	 * @param mailKeyIn
	 * @param nameIn
	 * @param adressIn
	 * @param zipIn
	 * @param cityIn
	 * @param contactIn
	 * @param teleIn
	 * @param sizeIn
	 * @param currentProviderIn
	 */
	public void addLead(String mailKeyIn, String nameIn, String adressIn,
			String zipIn, String cityIn, String contactIn, String teleIn,
			String sizeIn, String currentProviderIn) {
			Lead lead = new Lead(mailKeyIn, nameIn, adressIn, zipIn, cityIn,
				contactIn, teleIn, sizeIn, currentProviderIn);
			boolean addtolist = true;
		if (checkDuplicate(mailKeyIn)) {
			duplicate_leads.add(lead);
			addtolist = false;
		}
		if (!verifyEmail(mailKeyIn)) {
			countInvalMail++;
			invalid_mails.add(mailKeyIn);
			addtolist = false;
		}
		if (!verifyName(nameIn)) {
			countInvalName++;
			this.invalid_names.add(nameIn);
			addtolist = false;
		}
		if (!verifyPhone(teleIn)) {
			countInvalPhone++;
			this.invalid_phones.add(teleIn);
			addtolist = false;
		}

		if (!verifyZip(zipIn)) {
			countInvalZip++;
			this.invalid_zips.add(zipIn);
			addtolist = false;
		}
		if(addtolist){
			dict.put(mailKeyIn,lead) ;
		}
		

	}

	public int getSize() {
		return dict.size();
	}

	public int getDuplicateCount() {
		return countDup;
	}

	public List<Lead> getDuplicateLeads() {
		return this.duplicate_leads;
	}

	public int getInvalidMailCount() {
		return countInvalMail;
	}

	public List<String> getInvalidMails() {
		return this.invalid_mails;
	}

	public int getInvalidNameCount() {
		return countInvalName;
	}

	public int getInvalidPhoneCount() {
		return countInvalPhone;
	}

	public int getInvalidZipCount() {
		return countInvalZip;
	}


	/**
	 * Author: Rasmus Nim 
	 * Edited: 18th May 2015 
	 * Version: 1.0
	 * 
	 * @return true om det �r lagom m�nga leads
	 */
	public boolean checkSLA() {
		LeadProperties prop = new LeadProperties();
		return this.dict.size() >= prop.minLeads().get()
				&& this.dict.size() <= prop.maxLeads().get();
	}

	/**
	 * Author: Emil 
	 * Edited: 25 Maj 2015 
	 * Version: 1.0 Description: Retrieves and
	 * returns customers from the database that match a given lead.
	 */
	public List<Lead> getExistingCustomers() {

		List<Lead> existing_customers = new ArrayList<Lead>();
		Database db = new Database();
		Query query = new Query().setTable("llx_societe").count("*").limit(1);

		for (String key : this.dict.keySet()) {
			Lead lead = this.dict.get(key);
			ResultSet customer = db.sendSelectQuery(query
					.where("name", lead.name).where("email", lead.email)
					.getSelectQuery());
			try {
				if (!customer.isBeforeFirst()) {
					// Take action here. Encountered a dupe.
					existing_customers.add(lead);
				}
			} catch (SQLException e) {
				e.printStackTrace(System.out);
			}
		}
		return existing_customers;
	}

	public List<String> getInvalidNames() {
		return this.invalid_names;
	}

	public List<String> getInvalidPhones() {
		return this.invalid_phones;
	}

	public Lead[] getLeadsArray() {
		return dict.values().toArray(new Lead[0]);
	}
}
