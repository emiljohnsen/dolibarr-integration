package App;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XmlHandling {
	
	 /**
		 *Author: Rasmus Nim
		 *Edited: 13st Maj 2015
		 *Version: 1.0
		 *Description: adds timestamp to xmlfile
	  * @param docIn document som ska sparas
	  * @throws Exception
	  */
		public void saveDocToDisk(Document docIn) throws Exception {
			Document doc = docIn;
			Element timestamp = doc.createElement("Timestamp");
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			timestamp.appendChild(doc.createTextNode(sdf.format(date)));
			doc.getFirstChild().appendChild(timestamp);
			
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			Result output = new StreamResult(new File("oldlist.xml"));
			Source input = new DOMSource(doc);
			
			transformer.transform(input, output);
		}
		 /**
		  *Author: Rasmus Nim
		 *Edited: 13st Maj 2015
		 *Version: 1.0
		 *Description: Checks old list, return percent of list containing old data
		  * @param docIn
		  * @return procent of list equals old list
		  * @throws Exception
		  */
		public float checkOldList(Document docIn) throws Exception{
			float x = 0; 
			NodeList elmold = getOldList().getElementsByTagName("lead");
			NodeList elmnew = docIn.getElementsByTagName("lead");
			for (int i = 0; i<elmnew.getLength(); i++){
				for(int k = 0 ; k<elmold.getLength(); k++){
					if(elmold.item(k).getLastChild().getTextContent().equals(elmnew.item(i).getLastChild().getTextContent())){
						x++;
					}
				}
			}
			return x/(float)elmnew.getLength();
		}
		
		/**
		 *Author: Rasmus Nim
		 *Edited: 13st Maj 2015
		 *Version: 1.0
		 *Description: build xml from string
		 * @param xml
		 * @return document
		 * @throws Exception
		 */
		public Document loadXMLFromString(String xml) throws Exception
		{
		    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		    factory.setNamespaceAware(true);
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    return builder.parse(new ByteArrayInputStream(xml.getBytes()));
		}
		
		/**
		 *Author: Rasmus Nim
		 *Edited: 13st Maj 2015
		 *Version: 1.0
		 *Description: get old disk from hd
		 * @return XmlDocument oldlist
		 * @throws Exception
		 */
		public Document getOldList() throws Exception{
			byte[] encoded = Files.readAllBytes(Paths.get("oldlist.xml"));
			String xmltext =  new String(encoded, StandardCharsets.UTF_8);
			return loadXMLFromString(xmltext);
		}
		
		/**
		 *Author: Rasmus Nim
		 *Edited: 13st Maj 2015
		 *Version: 1.0
		 *Description: check date on old list
		 * @return Date for old list
		 * @throws Exception
		 */
		@SuppressWarnings("deprecation")
		public Date checkListDate() throws Exception {
			Document doc = getOldList();
			return new Date(doc.getElementsByTagName("Timestamp").item(0).getTextContent());
		}

}
