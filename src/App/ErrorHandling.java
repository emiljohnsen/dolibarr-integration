/**
   *Author: Rasmus och Emil
   *Edited: 4st Maj 2015
   *Version: 1.0
   *Description: Hanterar programmets exceptions och bryter och mailar vid behov
 */

package App;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import App.schedule.Schedule;
import App.statistics.Statistics;

import org.w3c.dom.*;

import UnitTesting.ServerConnectionCloneClass;
import App.AppException.ErrorCode;
import App.mail.Mail;
import App.properties.LeadProperties;
import App.properties.MailProperties;

public class ErrorHandling {
	private static final String SLA_CHECK_ERROR_MESSAGE = "Uppfyller inte SLA kraven";
	private static final AppException.ErrorCode DUPLICATE_CUSTOMERS_ERROR_CODE = ErrorCode.SUCCESS;
	private static final String DUPLICATE_CUSTOMERS_ERROR_MESSAGE = "Duplicate customers";

	public ArrayList<AppException> errorList = new ArrayList<AppException>();
	public Leads leads = new Leads();
	public Database database = new Database();

	private Statistics stats = new Statistics();
	
	public int SLAMIN;
	public int SLAMAX;
	
	public static void main(String[] args){
		if (args.length > 0)
		{
			if (args[0].equals("-s") || args[0].equals("--schedule"))
			{
				System.exit(new Schedule(10).exit);
			}
		}
		ErrorHandling eh = new ErrorHandling();
		eh.run();
	}
	
	/**
	 	*Author: Jonathan Lind
	   *Edited: 18th Maj 2015 av rasmus
	   *Version: 1.1
	   * Description: l�ser nu in max och min fr�n properties filen.
	 */
	public ErrorHandling(){
		LeadProperties prop = new LeadProperties();
		SLAMAX = prop.maxLeads().get();
		SLAMIN = prop.minLeads().get();
	}
	
	/**
	   *Author: Emil Johnsen, Rasmus Nim
	   *Edited: 1 june 2015
	   *Version: 1.3
	   *Description: Executes application
	 */
	public void run(){


		ServerConnectionCloneClass test = new ServerConnectionCloneClass();
		AppException ERROR;
		ERROR = addLeadsList(test.getXmlCorruptPhone());
		logDuplicates();
		logInvalidMails();
		logInvalidNames();
		logInvalidPhones();
		logInvalidZip();
		if(ERROR.CODE.getValue() != 0){
			errorList.add(ERROR);
			errorAction(ERROR);
		}

		ERROR = checkCustomerDuplicates();
		if(ERROR.CODE.getValue() != 0){
			errorList.add(ERROR);
			errorAction(ERROR);
		}

		// Doesn't get called after checkSLA.
		stats.write();
		ERROR = checkSLA();
		if(ERROR.CODE.getValue() != 0){
			errorList.add(ERROR);
			errorAction(ERROR);
		}
		
		//Os�ker n�r i ordningen denna ska utf�ras, om den lyckas kommer den spara nya listan som gammal
		//vilket kan bli fel om den sparar s� m�ste progrmmet k�ras igen, d� kommer den skriva att listan �r samma. 
		//men den skriver i loggen mot vilket datum den kollade
 		ERROR = checkOldList(test.getXmlWithoutError());
		if(ERROR.CODE.getValue() != 0){
			errorList.add(ERROR);
			errorAction(ERROR);
		}
		
		ERROR = deleteLeadsDolibarr();
		if(ERROR.CODE.getValue() != 0){
			errorList.add(ERROR);
			errorAction(ERROR);
		}
		 
		 
		ERROR = addLeadsDolibarr();
		if(ERROR.CODE.getValue() != 0){
			errorList.add(ERROR);
			errorAction(ERROR);
		}
		handleErrorList();
	}
	/**
	*Author: Rasmus Nim
	   *Edited: 1 june 2015
	   *Version: 1.0
	   *Description: Ska ta hand om listan med appexception n�r den �r klar...
	 */
	 private void handleErrorList() {
		 for (AppException ae : errorList){
			 if(ae.severity == Severity.HIGH){
				 this.sendMail(new MailProperties().list("high").toArray(), "Error in something, check logg");
				 return;
			 }
		 }
	}

	/**
	   *Author: Rasmus Nim
	   *Edited: 1 june 2015
	   *Version: 1.0
	   *Description: Kollar gamla listan, sparar �ver den och loggar hur mycket som var samma och vilket datum den kollar mot.
	  * @param docIn
	  * @return
	  */
	public AppException checkOldList(Document docIn) {
		try {
			XmlHandling xml = new XmlHandling();
			float percent = xml.checkOldList(docIn);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			Date date = xml.checkListDate();
			xml.saveDocToDisk(docIn);
			if(percent < 99){
				return new AppException(Severity.NON, percent + "% of old list are the same and where from " + sdf.format(date), AppException.ErrorCode.SUCCESS);
			}else{
				return new AppException(Severity.HIGH, percent + "% of old list are the same and where from " + sdf.format(date), AppException.ErrorCode.UNKNOWN);
			}
		} catch (Exception e) {
			return new AppException(Severity.MEDIUM, "Could not check old list", AppException.ErrorCode.UNKNOWN);
		}
		 
	}

	/**
	   *Author: Rasmus Nim
	   *Edited: 2 june 2015
	   *Version: 1.0
	   *Description: Errors take action!
	 * @param e
	 */
	public void errorAction(AppException e) {
		if(e.getSeverity()==Severity.FATAL){	
			sendMail(new MailProperties().list("fatal").toArray(), "Fatal error, see logg");
			new Schedule(10);
			System.exit(0);
		}
	}

	/**
	   *Author: Rasmus och Emil
	   *Edited: 4st Maj 2015
	   *Version: 1.0
	   *Description: S�ger �t mailhandling att skicka mail till de som beh�vs
	 * @param mailListIn 
	 */
	public void sendMail(String[] mailListIn, String message){
		new Mail().send("Error message", message, mailListIn);
	}
	
	/**
	 * Author: Jonathan Lind
	 * Edited: 12th May 2015
	 * Version: 1.0
	 * Description: Fetching the XML document containing the leads from ServerConnection.
	 * @return Returns an XML Document with the leads
	 */
	public Document fetchLeadsList(){
		ServerConnection getLeads = new ServerConnection();
		return getLeads.getXmlFromLeads();
	}

	/**
	 *Author: Emil
	 *Edited: 19 Maj 2015
	 *Version: 1.0
	 *Description: Anropar leads.checkSLA(), skickar tillbaka en AppException beroende p� resultat.
	 */
	public AppException checkSLA(){
		return (leads.checkSLA()) ? new AppException(Severity.NON, "Succeeded", AppException.ErrorCode.SUCCESS) : new AppException(Severity.FATAL, SLA_CHECK_ERROR_MESSAGE, AppException.ErrorCode.SLA_FAIL);
	}

	/**
	 *Author: Emil
	 *Edited: 25 Maj 2015
	 *Version: 1.3
	 *Description: Gets list of leads who are already customers. If duplicates, log them and remove from leads.
	 * Return exception.
	 */
	public AppException checkCustomerDuplicates(){
		List<Lead> existing_customers = this.leads.getExistingCustomers();

		stats.add("duplicate_customers", existing_customers.size(), this.leads.getSize());
		if (existing_customers.size() == 0)
		{
			return new AppException(Severity.NON, "No duplicate customers", AppException.ErrorCode.SUCCESS);
		}
		else
		{
			List<String> log_message = new ArrayList<String>();
			log_message.add(DUPLICATE_CUSTOMERS_ERROR_MESSAGE + existing_customers.size());
			for (Lead l : existing_customers)
			{
				String[] lead = new String[]{l.email, l.name, l.adress, l.zip, l.city, l.contact, l.tele, l.size, l.currentProvider};
				this.leads.removeLeadByKey(l.email);
				log_message.add("\n<" + String.join(", ", lead) + ">");
			}


			Logger.append(Severity.MEDIUM, String.join("", log_message), DUPLICATE_CUSTOMERS_ERROR_CODE);
			return 	new AppException(Severity.MEDIUM, DUPLICATE_CUSTOMERS_ERROR_MESSAGE, DUPLICATE_CUSTOMERS_ERROR_CODE);

		}
	}
	
	/**
	 * Author: Jonathan Lind
	 * Edited: 12th May 2015
	 * Version: 1.0
	 * @return Size of leads list is returned
	 */
	public int countLeads(){
		return leads.getSize();
	}
	
	/**
	 * Author: Jonathan Lind
	 * Edited: 12th May 2015
	 * Version: 1.0
	 * Description: Adds the leads to the lead-object list that later will be imported to Dolibarr
	 * @param leadsXML XML Document that is fetched from Web Scraper
	 * @return AppException to provide information of any possible errors that might have occured
	 */
	public AppException addLeadsList(Document leadsXML){
	    Element docEle = leadsXML.getDocumentElement();
	    NodeList nl = docEle.getChildNodes();
	    if (nl != null && nl.getLength() > 0) {
	        for (int i = 0; i < nl.getLength(); i++) {
	            if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
	                Element el = (Element) nl.item(i);
	                if (el.getNodeName().contains("lead")) {
	                	try {
	                    String name = new String(el.getElementsByTagName("name").item(0).getTextContent().getBytes(), "UTF-8");
	                    String address = new String(el.getElementsByTagName("address").item(0).getTextContent().getBytes(), "UTF-8");
	                    String zip = el.getElementsByTagName("zip").item(0).getTextContent();
	                    String city = new String(el.getElementsByTagName("city").item(0).getTextContent().getBytes(), "UTF-8");
	                    String contact = el.getElementsByTagName("contact").item(0).getTextContent();
	                    String tele = el.getElementsByTagName("tele").item(0).getTextContent();
	                    String size = el.getElementsByTagName("size").item(0).getTextContent();
	                    String current_provider = new String(el.getElementsByTagName("current_provider").item(0).getTextContent().getBytes(), "UTF-8");
	                    String email = new String(el.getElementsByTagName("email").item(0).getTextContent().getBytes(), "UTF-8");
	                    leads.addLead(email, name, address, zip, city, contact, tele, size, current_provider);
	                    	
						} catch (UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
	                    
	                    
	                }
	            }
	        }
	    }
	    
    	return new AppException(Severity.NON, "Successful add", AppException.ErrorCode.SUCCESS);
	    
	}

	/**
	 * author: emil
	 * version: 1.1
	 * date: 1 june 2015
	 * description: log duplicate leads, write percentage of duplicates in statistics file.
	 * @return
	 */
	public AppException logDuplicates(){
		List<String> exception_message = new ArrayList<String>();
		exception_message.add("Duplicates in leads list: " + this.leads.getDuplicateCount());
		int duplicate_leads = 0;
		for (Lead l : leads.getDuplicateLeads())
		{
			duplicate_leads++;
			String[] lead = new String[]{l.email, l.name, l.adress, l.zip, l.city, l.contact, l.tele, l.size, l.currentProvider};
			exception_message.add("\n<" + String.join(", ", lead) + ">");
		}
		stats.add("duplicates", duplicate_leads, leads.getSize());
		return new AppException(Severity.NON, String.join("", exception_message));
	}
	
	public AppException logInvalidMails(){
		float invalid_mails_count =  this.leads.getInvalidMailCount();
		List<String> exception_message = new ArrayList<String>();
		exception_message.add("Invalid entries in leads list: " + invalid_mails_count+ "\n<");

		for (String s : leads.getInvalidMails())
		{
			exception_message.add("\n" + s);
		}
		exception_message.add(">\n");

		stats.add("invalid_mails", invalid_mails_count, this.leads.getSize());

		return new AppException(Severity.NON, String.join("", exception_message), AppException.ErrorCode.SUCCESS);
	}
	
	public AppException logInvalidNames(){

		float invalid_name_count = this.leads.getInvalidNameCount();
		List<String> exception_message = new ArrayList<String>();
		exception_message.add("Invalid names in leads list: " + invalid_name_count + "\n<");

		for (String s : leads.getInvalidNames())
		{
			exception_message.add("\n" + s);
		}
		exception_message.add(">\n");

		stats.add("invalid_names", invalid_name_count, this.leads.getSize());

		return new AppException(Severity.NON, String.join("", exception_message), AppException.ErrorCode.SUCCESS);
	}
	
	public AppException logInvalidPhones(){

		List<String> exception_message = new ArrayList<String>();
		int invalidPhoneCount = this.leads.getInvalidPhoneCount();
		exception_message.add("Invalid phones in leads list: " + invalidPhoneCount + "\n<");

		for (String s : leads.getInvalidPhones())
		{
			exception_message.add("\n" + s);
		}
		exception_message.add(">\n");
		stats.add("invalid_names", invalidPhoneCount, this.leads.getSize());
		return new AppException(Severity.NON, String.join("", exception_message), AppException.ErrorCode.SUCCESS);
	}
	
	public AppException logInvalidZip(){

		int invalidZipCount = leads.getInvalidZipCount();
		stats.add("invalid_names", invalidZipCount, this.leads.getSize());
		return new AppException(Severity.NON, invalidZipCount + " Invalid names in leads list", AppException.ErrorCode.SUCCESS);
	}
	
	/**
	 * Author: Rasmus Nim
	 * Edited: 26th May 2015
	 * Version: 1.0
	 * Description: f�rs�ker skicka in alla leads i databasen.
	 * @return
	 */
	public AppException addLeadsDolibarr(){
		return database.insertLeadsToDatabase(leads.getLeadsArray());		
	}
	
	/**
	 * Author: Rasmus Nim
	 * Edited: 26th May 2015
	 * Version: 1.0
	 * Description: T�mmer databasen fr�n leads
	 * @return
	 */
	public AppException deleteLeadsDolibarr(){
		return database.deleteAllLeads();
	}
}
