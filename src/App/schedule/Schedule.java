package App.schedule;

import App.properties.ScheduleProperties;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 *Author: Emil
 *Edited: 27 Maj 2015
 *Version: 1.0
 * Creates cron job based on properties.ini
 */
public class Schedule {

    private ScheduleProperties properties = new ScheduleProperties();
    public int exit = 0;

    private String cron_cmd = "cat < (crontab -l) |grep -v \"{CMD}\" < (echo \"{CMD}\")";

    public Schedule()
    {
        Map<String, Integer> days = new HashMap<String, Integer>()
        {{
            put("Sun", 0);
            put("Mon", 1);
            put("Tue", 2);
            put("Wed", 3);
            put("Thu", 4);
            put("Fri", 5);
            put("Sat", 6);
        }};

        String[] time_day = properties.runTime().get().split(" ");

        String minute = time_day[0].split(":")[1];
        String hour = time_day[0].split(":")[0];
        Integer day = days.get(time_day[1]);
        String cron = properties.cron().get();
        String cmd = String.join(" ", new String[]{minute, hour, "*", "*", day.toString(), cron});
        try {
            Runtime.getRuntime().exec(this.cron_cmd.replace("{CMD}", cmd));
        } catch (IOException e) {
            this.exit = -1;
        }
    }

    /**
     * Author: Emil
     * Edited: 27 Maj 2015
     * Version: 1.0
     * Schedule program to run again at n minutes from now.
     */
    public Schedule(int n)
    {
        String cmd = "echo " + this.properties.run_program_path().get() + " | at now + " + n + " min ";
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            this.exit = -1;
        }
    }
}
