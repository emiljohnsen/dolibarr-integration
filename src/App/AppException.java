

package App;
/**
 *Author: Rasmus och Emil
 *Edited: 4st Maj 2015
 *Version: 1.0
 *Description: An exception from the program
*/
import App.Logger;

@SuppressWarnings("serial")
public class AppException extends Exception {
	public  Severity severity; 
	public int ID;
	public ErrorCode CODE;



	public AppException(Severity severityIn, String errorMessageIn){
		super(errorMessageIn);
		this.severity = severityIn;
		this.CODE = ErrorCode.SUCCESS;
		
	}
	public AppException(Severity severityIn, String errorMessageIn, ErrorCode codeIn){
		super(errorMessageIn);
		this.severity = severityIn;
		this.CODE = codeIn;
		Logger.append(severityIn, errorMessageIn, codeIn);
		
	}

	public Severity getSeverity(){
		return severity;
	}
	
	public enum ErrorCode {
		SUCCESS(0),UNKNOWN(1),
	    NO_LEADS(11), LOW_LEADS(12), HIGH_LEADS(13), NO_CONTACT(14),
	    SQL_INJECT(21), JS_INJECT(22), INC_STANDARD(23), 
	    SLA_FAIL(41), 
	    DOLIBARR_CON_FAIL(51), DOLIBARR_READ_FAIL(52), DOLIBARR_READ_FAIL_SEP(53), DOLIBARR_BULK_FAIL(54),
	    VALID_LEADS_LOW(61),
	    UPLOAD_FAIL(71), UPLOAD_FAIL_SEP(72),
	    NO_SUBJECT_ERROR_CODE(81), NO_MESSAGE_ERROR_CODE(82), NO_RECIPIENTS_ERROR_CODE(83), ENCODING_EXCEPTION_ERROR_CODE(84), 
	    MESSAGING_EXCEPTION_ERROR_CODE(85), ADDRESS_EXCEPTION_ERROR_CODE(86);

	    private final int id;
	    ErrorCode(int id) { this.id = id; }
	    public int getValue() { return id; }
	}
}
