package App.statistics;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Statistics
{
    private Map<String, Float> statistics = new HashMap<String, Float>();


    public Statistics()
    {

    }

    private float frac(float numerator, float denominator) { return (denominator > 0.0) ? numerator/denominator : -1; }

    public Statistics add(String key, float numerator, float denominator)
    {
        System.out.print(key);
        System.out.print(" " + numerator);
        System.out.print(" " + denominator);
        System.out.print(" " + this.frac(numerator, denominator));
        System.out.println();

        this.statistics.put(key, this.frac(numerator, denominator));
        return this;
    }

    public void write()
    {
        StringBuilder builder = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat(("yyyy-MM-dd_HHmm"));
        Calendar cal = Calendar.getInstance();
        String today = sdf.format(cal.getTime());
        String file = "statistik_" + today + ".ini";

        builder.append("STATISTIK ").append(today).append("\n");
        statistics.forEach((k, v) -> {builder.append(k + " = " + ((v > -1) ? v*100 + "%" : "inga leads") + "\n"); });

        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream( file), "UTF-8"));
            writer.write(builder.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {writer.close();} catch (Exception ex) {ex.printStackTrace();}
        }
    }
}

