package App;

public class Lead {
	//mail �r key i mappen
	public String email;
	public String name;
	public String adress;
	public String zip;
	public String city;
	public String contact;
	public String tele;
	public String size;
	public String currentProvider;
	
	/**
	 * Author: Rasmus Nim
	 * Edited: 11th May 2015
	 * Version 1.0
	 * Description: Sets leads information for a lead
	 * @param mailKeyIn Mail Address for leads and also acts as the key value
	 * @param nameIn Company name for lead
	 * @param adressIn Address for lead
	 * @param zipIn Zip code for lead
	 * @param cityIn City for lead
	 * @param contactIn Contact name for lead
	 * @param teleIn Phone number for lead
	 * @param sizeIn Size of lead
	 * @param currentProviderIn Current provider for lead
	 */
	public Lead(String mailKeyIn, String nameIn, String adressIn, String zipIn, String cityIn, 
			String contactIn, String teleIn,String sizeIn, String currentProviderIn){
		email=mailKeyIn;
		name = nameIn;
		adress = adressIn;
		zip=zipIn;
		city=cityIn;
		contact = contactIn;
		tele=teleIn;
		size=sizeIn;
		currentProvider = currentProviderIn;
	}
	
	/**
	 * Author: Rasmus Nim
	 * Edited: 11th May 2015
	 * Version: 1.0
	 * Description: returns the email address of a lead
	 * @return returns the email of a lead
	 */
	public String getEmail() {
		return email;
	}
}
